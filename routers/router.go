package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ishangoyal/randomdata/models"
)

func RegisterRoutes() *gin.Engine {
	router := gin.Default()

	r1 := router.Group("/users")
	{
		r1.GET("/", getUsers)
		r1.GET("age/:age", getUsersByAge)
		r1.GET("profiles/:firstname/:age", getUsersByAgeandfirstname)
		r1.GET("/v1/:state/:city", getUsersByStateandCity)
		r1.GET("/v2/:age/:state/:city", getUsersByStateandCityandage)
	}

	router.POST("/user", postUser)

	router.PUT("/user/:id", updateKyc)
	router.PUT("/users/:id", updateUser)

	r2 := router.Group("/users")
	{
		r2.DELETE("/", DeleteUsers)
		r2.DELETE("email/:email", deleteByEmail)
		r2.DELETE("age/:age", deleteByAge)
		r1.DELETE("/v1/:state/:city", DeleteUsersByStateandCity)
		r2.DELETE("profiles/:firstname/:age", deleteUsersByAgeandfirstname)
	}

	return router
}

// GET routes

func getUsers(c *gin.Context) {
	var allUsers []models.User
	models.DB.Preload("Kyc").Find(&allUsers)

	c.IndentedJSON(http.StatusOK, gin.H{"allUsers": allUsers})
}

func getUsersByAge(c *gin.Context) {
	var allValidUser []models.User
	res := models.DB.Preload("Kyc").Where("age = ?", c.Param("age")).Find(&allValidUser)

	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"dbdata": allValidUser})
	}
}

func getUsersByAgeandfirstname(c *gin.Context) {
	var allValidUser []models.User
	res := models.DB.Preload("Kyc").Where("age = ? and first_name = ?", c.Param("age"), c.Param("firstname")).Find(&allValidUser)

	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"dbdata": allValidUser})
	}
}

func getUsersByStateandCity(c *gin.Context) {
	var allValidUser []models.User

	res := models.DB.Preload("Kyc").Raw(`SELECT * FROM users as u LEFT JOIN kycs as k ON u.kyc_id = k.id Where k.state = ? and city = ?`, c.Param("state"), c.Param("city")).Find(&allValidUser)

	if res.RowsAffected == 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"response": "Record not found"})
		return
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"dbdata": allValidUser})
	}

}

func getUsersByStateandCityandage(c *gin.Context) {
	var userDb []models.User

	res := models.DB.Preload("Kyc").Raw(`SELECT * FROM users as u LEFT JOIN kycs as k ON u.kyc_id = k.id Where u.age = ? and k.state = ? and city = ?`, c.Param("age"), c.Param("state"), c.Param("city")).Find(&userDb)

	if res.RowsAffected == 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"response": "Record not found"})
		return
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"dbdata": userDb})
	}

}

// POST route

func postUser(c *gin.Context) {
	var newuser models.User
	if err := c.ShouldBindJSON(&newuser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	models.DB.Preload("Kyc").Create(&newuser)
	c.IndentedJSON(http.StatusOK, gin.H{"message": "User created successfully"})
}

// PUT routes

func updateKyc(c *gin.Context) {
	var update models.Kyc
	var database models.User

	if err := c.BindJSON(&update); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res := models.DB.Model(&database.Kyc).Where("id = ?", c.Param("id")).Updates(update)
	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"updated_user": "All users updated successfuly"})
	}
}

func updateUser(c *gin.Context) {
	var update models.User
	var database models.User

	if err := c.BindJSON(&update); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res := models.DB.Model(&database).Where("id = ?", c.Param("id")).Updates(update)
	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"updated_user": "All users updated successfuly"})
	}
}

// DELETE routes

func DeleteUsers(c *gin.Context) {
	var allUsers []models.User
	res := models.DB.Find(&allUsers)

	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		models.DB.Preload("Kyc").Delete(&allUsers)
		c.IndentedJSON(http.StatusOK, gin.H{"message": "All users deleted successfuly"})
	}

}

func deleteByEmail(c *gin.Context) {
	var delUser []models.User
	res := models.DB.Where("email = ?", c.Param("email")).Find(&delUser)

	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		models.DB.Preload("Kyc").Delete(&delUser)
		c.IndentedJSON(http.StatusOK, gin.H{"message": "users deleted successfuly"})
	}
}

func deleteByAge(c *gin.Context) {
	var delUser []models.User

	res := models.DB.Where("age = ?", c.Param("age")).Find(&delUser)

	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		models.DB.Preload("Kyc").Unscoped().Delete(&delUser)
		c.IndentedJSON(http.StatusOK, gin.H{"message": "users deleted successfuly"})
	}

}

func deleteUsersByAgeandfirstname(c *gin.Context) {
	var allValidUser []models.User
	res := models.DB.Where("age = ? and first_name = ?", c.Param("age"), c.Param("firstname")).Find(&allValidUser)

	if res.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	} else {
		models.DB.Preload("Kyc").Delete(&allValidUser)
		c.IndentedJSON(http.StatusOK, gin.H{"message": "All users deleted"})
	}
}

func DeleteUsersByStateandCity(c *gin.Context) {
	var allValidUser []models.User
	var noUser []models.Kyc
	res := models.DB.Where("state = ? and city = ?", c.Param("state"), c.Param("city")).Find(&noUser)

	if res.RowsAffected == 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"response": "Record not found"})
		return
	} else {
		models.DB.Preload("Kyc", "state = ? and city = ?", c.Param("state"), c.Param("city")).Delete(&allValidUser)
		c.IndentedJSON(http.StatusOK, gin.H{"dbdata": allValidUser})
	}
}
