package models

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type User struct {
	ID          uint   `gorm:"primary key:autoIncrement" json:"id"`
	FirstName   string `json:"firstname"`
	LastName    string `json:"lastname"`
	Age         int    `json:"age"`
	Email       string `json:"email"`
	PhoneNumber uint64 `json:"phoneNumber"`
	KycID       int    `json:"-"`
	Kyc         Kyc    `gorm:"foreignKey:KycID;references:ID;constraint:OnDelete:CASCADE;" json:"kyc"`
}

type Kyc struct {
	ID           uint   `json:"-"`
	AadharNumber uint64 `gorm:"unique" json:"aadharNumber"`
	Pancard      string `json:"pancard"`
	State        string `json:"state"`
	Pincode      string `json:"pincode"`
	City         string `json:"city"`
	Address      string `json:"address"`
}

var DB *gorm.DB

func ConnectDatabase() {
	godotenv.Load(".env")
	connectionString := fmt.Sprintf("host=%s user=%s dbname=%s password=%s sslmode=%s", os.Getenv("HOST"), os.Getenv("USER"), os.Getenv("DBNAME"), os.Getenv("PASSWORD"), os.Getenv("SSLMODE"))
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: connectionString,
	}))
	if err != nil {
		panic("Error:Failed to connect to database!")
	}
	// db.Exec(`CREATE DATABASE userData`)
	db.AutoMigrate(&User{}, &Kyc{})

	DB = db
}
