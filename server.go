package main

import (
	"fmt"

	"gitlab.com/ishangoyal/randomdata/models"
	"gitlab.com/ishangoyal/randomdata/routers"
)

func main() {
	r := routers.RegisterRoutes()
	models.ConnectDatabase()

	fmt.Println("Server is running on 8080 :)")
	r.Run(":8080")
}
